#include <algorithm>
#include <numeric>
#include <iostream>
#include <string>
#include <vector>
#include <cassert>
#include <queue>
#include <mutex>

#include "catch.hpp"

using namespace std;

TEST_CASE("if with initializer")
{
	vector vec = { 2, 45, 235, 55, 22, 5345, 665, 3545, 234 };

	if (auto pos = find(begin(vec), end(vec), 665); pos != end(vec))
	{
		cout << "Item has been found " << *pos << '\n';
	}
	else
	{
		cout << "Item was not found\n";
		assert(pos == end(vec));
	}
}

TEST_CASE("use case with mutex")
{
	queue<int> q;
	mutex q_mtx;

	int value{};

	SECTION("Before C++17")
	{		
		{
			lock_guard<mutex> lk{ q_mtx }; // q_mtx.lock()

			if (!q.empty())
			{
				value = q.front();
				q.pop();
			}
		} // q_mtx.unlock()

		//... process(value);
	}

	SECTION("Since C++17")
	{
		if (lock_guard lk{ q_mtx }; !q.empty())
		{
			value = q.front();
			q.pop();
		} // q_mtx.unlock()

		//... process(value);
	}
}