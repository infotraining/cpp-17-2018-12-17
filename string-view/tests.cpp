#include <algorithm>
#include <numeric>
#include <iostream>
#include <string>
#include <vector>
#include <string_view>

#include "catch.hpp"

using namespace std;

template <typename Container>
void print(const Container& c, string_view prefix)
{
	cout << prefix << ": [ ";
	for (const auto& item : c)
		cout << item << " ";
	cout << ']\n';
}

TEST_CASE("print")
{
	vector vec = { 1, 2, 3 };
	print(vec, "vec"s);
}

string_view get_prefix(string_view text)
{
	return string_view(text.data(), 2);
}

TEST_CASE("unsafe code")
{
	auto text = "aabb";
	auto prefix = get_prefix(text);

	REQUIRE(prefix == "aa"sv);

	REQUIRE(get_prefix("aabb"s) == "aa"sv); // !!! Bug
}

TEST_CASE("string_view")
{
	SECTION("construction")
	{
		SECTION("explicit")
		{
			auto text = "ala ma kota";

			string_view sv1{ text, 3 };

			REQUIRE(sv1 == "ala"sv);
		}

		SECTION("implicit conversion")
		{
			string_view sv1 = "text";

			REQUIRE(sv1 == "text"sv);

			string str = "text in string";
			string_view sv2 = str;

			REQUIRE(sv2 == "text in string"sv);

			SECTION("Beware!!!")
			{
				string_view sv_bad = "text"s;

				cout << sv_bad << endl;
			}
		}
	}
}