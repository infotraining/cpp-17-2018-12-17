#include <algorithm>
#include <numeric>
#include <iostream>
#include <string>
#include <vector>

#include "catch.hpp"

using namespace std;

namespace BeforeCpp17
{
	// Head-Tail idiom

	template <typename Head>
	auto sum(Head head)
	{
		return head;
	}

	template <typename Head, typename... Tail>
	auto sum(Head head, Tail... tail)
	{
		return head + sum(tail...);
	}
}

template <typename... Args>
auto sum(Args&&... args)
{
	return (... + std::forward<Args>(args)); // unary left fold - ((((1 + 2) + 3) + 4) + 5)
}

template <typename... Args>
auto sum_right(Args... args)
{
	return (args + ...); // unary right fold - (1 + (2 + (3 + (4 + 5))))
}

TEST_CASE("fold expressions")
{
	auto result = BeforeCpp17::sum(1, 2, 3, 4, 5);
	REQUIRE(result == 15);

	result = sum(1, 2, 3, 4, 5);
	REQUIRE(result == 15);

	result = sum_right(1, 2, 3, 4, 5);
	REQUIRE(result == 15);
}

template <typename... Args>
void print_all(const Args&... args)
{
	bool is_first = true;

	auto with_space = [&is_first](const auto& arg) {
		if (!is_first)
			cout << " ";
		is_first = false;
		
		return arg;
	};

	(cout << ... << with_space(args)) << '\n'; // left binary fold
}

namespace Comma
{
	template <typename... Args>
	void print_all(const Args&... args)
	{
		((cout << args << " "), ...);
		cout << '\n';
	}
}

TEST_CASE("print_all")
{
	print_all(1, 3.14, "test", "abc"s);
	Comma::print_all(1, 3.14, "test", "abc"s);
}