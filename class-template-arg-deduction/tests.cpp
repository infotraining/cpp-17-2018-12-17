#include <algorithm>
#include <numeric>
#include <iostream>
#include <string>
#include <vector>
#include <mutex>
#include <optional>
#include <list>
#include <array>

#include "catch.hpp"

using namespace std;

template <typename T>
void deduce1(T arg)
{
	puts(__FUNCSIG__);
}

template <typename T>
void deduce2(T& arg)
{
	puts(__FUNCSIG__);
}

template <typename T>
void deduce3(T&& arg)
{
	puts(__FUNCSIG__);
}

void foo(int)
{}

TEST_CASE("deduce size of array")
{
	int tab[10];	

	REQUIRE(size(tab) == 10);
}

TEST_CASE("type deduction rules")
{
	int x = 10;
	int& ref_x = x;
	const int& cref_x = x;
	int tab[10];

	SECTION("Case1 - const, volatile & refs are removed, array & func decays")
	{
		deduce1(x);
		auto ax1 = x; // int

		deduce1(ref_x);
		auto ax2 = ref_x;

		deduce1(cref_x);
		auto ax3 = cref_x;

		deduce1(tab);
		auto atab = tab; // int*

		deduce1(foo);
		auto afun = foo; // void(*)(int)
	}

	SECTION("Case2 - passing using ref - const, volatile, array size and function are preserved")
	{
		cout << "\n\n";

		deduce2(x);
		auto& ax1 = x; // int&

		deduce2(ref_x);
		auto& ax2 = ref_x; // int&
		
		deduce2(cref_x);
		auto& ax3 = cref_x; // const int&
		
		deduce2(tab);
		auto& atab = tab; // int(&)[10]
		
		deduce2(foo);
		auto& afun = foo; // void(&)(int)
	}

	SECTION("Case 3")
	{
		cout << "\n\n";

		deduce3(4);
		deduce3(x);
	}
}

template <typename T1 = int, typename T2 = unsigned char>
struct ValuePair
{
	T1 fst;
	T2 snd;

	ValuePair(T1 arg1, T2 arg2) : fst{arg1}, snd{arg2}
	{}
};

TEST_CASE("Class template arg deduction")
{
	ValuePair<int, double> vp1{ 1, 3.14 };

	SECTION("Since C++17")
	{
		ValuePair vp2{ 1, 3.14 };
		static_assert(is_same_v<decltype(vp2), ValuePair<int, double>>);

		ValuePair vp3{ 1, "text"s };
		static_assert(is_same_v<decltype(vp3), ValuePair<int, string>>);

		ValuePair vp4{ 3.14, "text" };
		static_assert(is_same_v<decltype(vp4), ValuePair<double, const char*>>);

		ValuePair vp5{ foo, 3.14f };
		static_assert(is_same_v<decltype(vp5), ValuePair<void(*)(int), float>>);		
	}
}

TEST_CASE("sort with deduction")
{
	vector vec = { 1, 2, 3, 4, 5 };

	sort(begin(vec), end(vec), greater());
}

TEST_CASE("locking with scoped lock")
{
	mutex mtx;
	timed_mutex tmtx;

	SECTION("before C++17")
	{

		unique_lock<mutex> lk1{ mtx, defer_lock };
		unique_lock<timed_mutex> lk2{ tmtx, defer_lock };

		lock(lk1, lk2);

		// critical section
	}

	SECTION("since C++17")
	{
		scoped_lock scl(mtx, tmtx); // scoped_lock<std::mutex, std::timed_mutex>;

		// critical section
	}
}

TEST_CASE("special case for CTAD")
{
	vector vec = { 1, 2, 3 };
	vector vec2{ vec };
	static_assert(is_same_v<decltype(vec2), vector<int>>);

	optional<int> opt = 1;
	optional opt2 = opt;
	static_assert(is_same_v<decltype(opt2), optional<int>>);
}


namespace Optimized
{
	template <typename T1, typename T2>
	struct ValuePair
	{
		T1 fst;
		T2 snd;

		template <typename Arg1, typename Arg2>
		ValuePair(Arg1&& arg1, Arg2&& arg2) 
			: fst{ std::forward<Arg1>(arg1) }, snd{ std::forward<Arg2>(arg2) }
		{}		
	};

	// deduction guide
	template <typename T1, typename T2> ValuePair(T1, T2) -> ValuePair<T1, T2>;
}

TEST_CASE("Optimized with universal refs")
{
	Optimized::ValuePair vp{ 1, 3.14 };
	static_assert(is_same_v<decltype(vp), Optimized::ValuePair<int, double>>);

	int x = 10;
	int& ref_x = x;

	Optimized::ValuePair vp2{ x, 1 };
	static_assert(is_same_v<decltype(vp2), Optimized::ValuePair<int, int>>);

	Optimized::ValuePair vp3{ 1, "text" };
	static_assert(is_same_v<decltype(vp3), Optimized::ValuePair<int, const char*>>);
}

TEST_CASE("deduction & std containers")
{
	vector vec = { 1, 2, 3, 4 }; // vector<int>

	list lst(begin(vec), end(vec)); // list<int>

	array arr = { 1, 2, 3, 4, 5, 6, 7, 8 }; // array<int, 8>
}