#include <algorithm>
#include <numeric>
#include <iostream>
#include <string>
#include <vector>

#include "catch.hpp"

using namespace std;

template <typename T>
string convert_to_string(T value)
{
	if constexpr (is_arithmetic_v<T>)
	{
		return std::to_string(value);
	}
	else if constexpr (is_same_v<T, std::string>)
	{
		return value;
	}
	else
	{
		return std::string{ value };
	}	
}

TEST_CASE("constexpr if")
{
	SECTION("convert from number")
	{
		string txt = convert_to_string(4);
		REQUIRE(txt == "4"s);
	}

	SECTION("convert from string")
	{
		string txt = convert_to_string("5"s);
		REQUIRE(txt == "5"s);
	}

	SECTION("convert from const char*")
	{
		string txt = convert_to_string("text");
		REQUIRE(txt == "text"s);
	}
}

template <typename T, size_t N>
void process_array(T(&tab)[N])
{
	if constexpr (N <= 255)
	{
		cout << "Processing small table...\n";
	}
	else
	{
		cout << "Processing large table...\n";
	}
}

TEST_CASE("processing arrays")
{
	SECTION("small")
	{
		int tab[128];

		process_array(tab);
	}

	SECTION("large")
	{
		int tab[1024];

		process_array(tab);
	}
}

namespace BeforeCpp17
{
	void print()
	{
		cout << "\n";
	}

	template <typename Head, typename... Tail>
	void print(const Head& head, const Tail&... tail)
	{
		cout << head << " ";
		print(tail...);
	}
}

template <typename Head, typename... Tail>
void print(const Head& head, const Tail&... tail)
{
	cout << head << " ";

	if constexpr (sizeof...(tail) > 0)
		print(tail...);
	else
		cout << "\n";
}

TEST_CASE("variadic templates")
{
	BeforeCpp17::print(1, 3.14, "test"s);
	BeforeCpp17::print(665);
	BeforeCpp17::print();

	print(9, "text", 3.14);
}




















