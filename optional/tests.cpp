#include <algorithm>
#include <numeric>
#include <iostream>
#include <string>
#include <vector>
#include <optional>
#include <complex>
#include <charconv>

#include "catch.hpp"

using namespace std;

TEST_CASE("optional")
{
	SECTION("construction")
	{
		SECTION("without state")
		{
			optional<int> opt1;
			REQUIRE(opt1.has_value() == false);

			optional<int> opt2 = nullopt;
			REQUIRE(opt2.has_value() == false);
		}

		SECTION("with a state")
		{
			optional<int> opt1 = 4;
			REQUIRE(opt1.has_value());

			optional opt2 = 4;
			static_assert(is_same_v<decltype(opt2), optional<int>>);
			REQUIRE(opt2.has_value());

			optional<complex<double>> opt_x{ in_place, 3.9, 2.1 };

			SECTION("getting a value - unsafe way")
			{

				if (opt_x)
				{
					REQUIRE(opt_x->real() == Approx(3.9));
					REQUIRE((*opt_x).imag() == Approx(2.1));
				}
			}

			SECTION("safe way")
			{
				auto x = opt_x.value();
				REQUIRE(x.real() == Approx(3.9));

				opt_x.reset();

				REQUIRE_THROWS_AS(opt_x.value(), bad_optional_access);
			}

			SECTION("value_or()")
			{
				optional<string> name;
				REQUIRE(name.value_or("(empty)") == "(empty)"s);

				name = "Jan";
				REQUIRE(name.value_or("(empty)") == "Jan"s);
			}
		}
	}
}

optional<int> to_int(string_view str)
{
	int result{};

	auto start = str.data();
	auto end = str.data() + str.size();

	if (auto[pos, error_code] = from_chars(start, end, result); error_code != std::errc{} || pos != end)
	{
		return std::nullopt;
	}

	return result;
}

TEST_CASE("to_int")
{
	SECTION("happy path")
	{
		auto n = to_int("123");

		REQUIRE(n.value() == 123);
	}

	SECTION("sad path")
	{
		auto n = to_int("12a4");

		REQUIRE(n.has_value() == false);
	}
}