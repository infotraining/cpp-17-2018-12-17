#include <algorithm>
#include <numeric>
#include <iostream>
#include <string>
#include <vector>
#include <variant>

#include "catch.hpp"

using namespace std;

TEST_CASE("variant")
{
	variant<int, double, string> var;

	REQUIRE(holds_alternative<int>(var));

	auto& item = get<int>(var);
	REQUIRE(item == 0);

	var = 2.13;
	var = "text"s;

	var = 'a'; // implicit conversion to int
	REQUIRE(holds_alternative<int>(var));

	REQUIRE(var.index() == 0);

	REQUIRE_THROWS_AS(get<string>(var), bad_variant_access);
}

struct PrintVisitor
{
	void operator()(int x) const
	{
		cout << "int(" << x << ")\n";
	}

	void operator()(double x) const
	{
		cout << "double(" << x << ")\n";
	}

	void operator()(const string& s) const
	{
		cout << "string(" << s << ")\n";
	}
};

template <typename... Ts>
struct Overloader : Ts...
{
	using Ts::operator()...;
};

template <typename... Ts>
Overloader(Ts...)->Overloader<Ts...>;

TEST_CASE("visiting variant")
{
	variant<int, double, string> var = "text"s;

	visit(PrintVisitor{}, var);

	auto& inline_printer = Overloader{
		[](int x) { cout << "int(" << x << ")\n"; },
		[](double x) { cout << "double(" << x << ")\n"; },
		[](const string& s) { cout << "string(" << s << ")\n"; }
	};

	var = 3.14;

	visit(inline_printer, var);
}

struct ErrorCode
{
	string msg;
	int errc;
};


[[nodiscard]] variant<string, ErrorCode> load_from_file(const string& file_name)
{
	if (file_name == "bad")
		return ErrorCode{ "bad file access", 13 };
	return "text"s;
}

TEST_CASE("handling a result")
{
	visit(Overloader{
		[](const string& content) { cout << "Content of file: " << content << '\n'; },
		[](ErrorCode& ec) { cout << "Error: " << ec.msg << '\n'; }
		}, load_from_file("bad"));
}




