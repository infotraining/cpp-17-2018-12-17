#include <algorithm>
#include <numeric>
#include <iostream>
#include <string>
#include <vector>
#include <array>

#include "catch.hpp"

using namespace std;

struct Gadget
{
	string id;
	int counter = 0;

	explicit Gadget(string id) : id{move(id)}
	{
		cout << "Gadget(" << this->id << ": " << this << ")\n";
	}

	Gadget(const Gadget& source) : id{ source.id }
	{
		cout << "Gadget(cc: " << this->id << ": " << this << ")\n";
	}

	Gadget(Gadget&& source) noexcept : id{ move(source.id) }
	{
		cout << "Gadget(mv: " << this->id << ": " << this << ")\n";
	}

	~Gadget()
	{
		cout << "~Gadget(" << this->id << ": " << this << ")\n";
	}

	void use()
	{
		++counter;
	}

	auto report()
	{
		/*
		return [=] { cout << "Hello from Gadget(" << id << " - " << this << ")\n"; };
		return [this] { cout << "Hello from Gadget(" << id << " - " << this << ")\n"; };
		return [&] { cout << "Hello from Gadget(" << id << " - " << this << ")\n"; };
		*/

		return[*this]() mutable {
			cout << "Hello from Gadget(" << id << " - " << ")\n"; 
			use();
		};
	}
};

TEST_CASE("capturing object to closure")
{
	function<void()> reporter;
	
	{
		Gadget g{ "ipad" };
		reporter = g.report();
	}

	reporter();
}

TEST_CASE("lambda expressions are constexpr")
{
	auto square = [](int x) { return x * x; };

	static_assert(square(8) == 64);

	int tab[square(8)];

	static_assert(size(tab) == 64);
}

namespace Cpp20
{
	template<typename Iter, typename Pred>
	constexpr Iter find_if(Iter first, Iter last, Pred pred)
	{
		Iter it = first;

		for (; it != last; ++it)
			if (pred(*it))
				return it;

		if (it == last)
			throw "item not found";		
	}
}

TEST_CASE("constexpr find_if")
{
	constexpr array<int, 8> data = { 1, 2, 3, 4, 665, 35, 253, 55 };

	constexpr auto value = *Cpp20::find_if(begin(data), end(data), [](int x) { return x >= 256; });

	static_assert(value == 665);

	constexpr auto by_factor = [value](int arg) constexpr { return value * arg; };

	static_assert(by_factor(2) == 1330);
}