#include <algorithm>
#include <numeric>
#include <iostream>
#include <string>
#include <vector>
#include <array>

#include "catch.hpp"

using namespace std;

struct SimpleAggregate
{
	int a;
	double b;
	int tab[3];

	void print() const
	{
		cout << "SimpleAggragate(" << a << ", " << b << ", [ ";
		for (const auto& item : tab)
			cout << item << " ";			
		cout << "])\n";
	}
};

TEST_CASE("aggregates")
{
	SimpleAggregate sa1{ 1, 3.14, { 1, 2 } };

	REQUIRE(sa1.a == 1);
	REQUIRE(sa1.b == Approx(3.14));
	sa1.print();

	SimpleAggregate sa2{ 1 };
	REQUIRE(sa2.a == 1);
	REQUIRE(sa2.b == Approx(0.0));
	sa2.print();

	SECTION("arrays")
	{
		int tab1[10] = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
		int tab2[10] = { 1, 2, 3 };
		int tab3[10] = {};
	}

	SECTION("std::array")
	{
		std::array<int, 3> arr1 = { {1, 2, 3} };
	}
}

struct ComplexAggregate : SimpleAggregate, std::string
{
	vector<int> vec;
};

TEST_CASE("complex aggragate")
{
	static_assert(is_aggregate_v<ComplexAggregate>);

	ComplexAggregate agg1{ {}, {"text"}, { 4, 5, 6 } };

	cout << agg1.c_str() << endl;
	agg1.print();

	REQUIRE(agg1.vec == vector{ 4, 5, 6 });
}

struct WTF
{
	int value = 5;

	WTF() = delete;
};

TEST_CASE("WTF")
{
	WTF wtf{};

	REQUIRE(wtf.value == 5);
}