#include <algorithm>
#include <numeric>
#include <iostream>
#include <string>
#include <vector>
#include <array>
#include <map>
#include <tuple>

#include "catch.hpp"

using namespace std;

namespace BeforeCpp17
{
	template <typename Container>
	auto calc_stats(const Container& data)
	{
		//typename Container::const_iterator min_it, max_it;
		decltype(begin(data)) min_it, max_it;
		tie(min_it, max_it) = std::minmax_element(begin(data), end(data));

		double avg = std::accumulate(std::begin(data), std::end(data), 0.0) / data.size();

		return tuple<int, int, double>(*min_it, *max_it, avg);
		// return make_tuple(*minmax_result.first, *minmax_result.second, avg);
	}
}

template <typename Container>
auto calc_stats(const Container& data)
{	
	auto [min_it, max_it] = std::minmax_element(begin(data), end(data));

	double avg = std::accumulate(std::begin(data), std::end(data), 0.0) / std::size(data);

	return tuple(*min_it, *max_it, avg);	
}


TEST_CASE("Before C++17")
{
	vector<int> vec = { 5, 42, 665, 1, 434, 342, 12 };

	SECTION("assign to var using auto")
	{
		auto result = BeforeCpp17::calc_stats(vec);

		REQUIRE(get<0>(result) == 1);
		REQUIRE(get<1>(result) == 665);
		REQUIRE(get<2>(result) == Approx(214.428));
	}

	SECTION("assign to var using tie")
	{
		int min, max;		

		tie(min, max, std::ignore) = BeforeCpp17::calc_stats(vec);

		REQUIRE(min == 1);
		REQUIRE(max == 665);		
	}
}

class Date
{
public:
	int year;
	std::string month;
	int day;
};

Date get_date()
{
	return Date{ 2018, "Dec", 17 };
}

TEST_CASE("structured bindings")
{
	SECTION("allow to unpack an array")
	{
		auto get_coord = []() -> int(&)[3]{
			static int coord[] = { 1, 2, 3 };
			return coord;
		};

		auto[x, y, z] = get_coord();

		REQUIRE(x == 1);
		REQUIRE(y == 2);
		REQUIRE(z == 3);

		int position[3] = { 4, 5, 6 };

		auto[pos_x, pos_y, pos_z] = position;
		REQUIRE(pos_x == 4);
	}

	SECTION("allow to unpack a tuple")
	{
		vector vec = { 5, 42, 665, 1, 434, 342, 12 };

		auto[min, max, avg] = calc_stats(vec);

		REQUIRE(min == 1);
		REQUIRE(max == 665);
		REQUIRE(avg == Approx(214.428));
	}

	SECTION("allow to unpack std::array")
	{
		std::array<int, 3> arr = { 1, 2, 3 };

		auto[x, y, z] = arr;

		REQUIRE(x == 1);
		REQUIRE(y == 2);
		REQUIRE(z == 3);
	}

	SECTION("allow to unpack a struct")
	{
		auto[year, month, day] = get_date();

		REQUIRE(year == 2018);
		REQUIRE(month == "Dec"s);
		REQUIRE(day == 17);
	}
}

TEST_CASE("how it works")
{
	vector vec = { 1, 2, 3 };

	auto[min, max, avg] = calc_stats(vec);

	SECTION("works like this")
	{
		auto unnamed_obj = calc_stats(vec);
		auto& min = get<0>(unnamed_obj);
		auto& max = get<1>(unnamed_obj);
		auto& avg = get<2>(unnamed_obj);
	}
}

TEST_CASE("gcc/clang vs visual studio")
{
	vector vec = { 1, 2, 3 };
	auto&& result = calc_stats(vec);
}

TEST_CASE("const, volatile, alignas & refs")
{
	vector vec = { 1, 2, 3 };

	SECTION("const auto&")
	{
		const auto&[min, max, avg] = calc_stats(vec);
	}

	SECTION("const auto")
	{
		const auto[min, max, avg] = calc_stats(vec);
	}

	SECTION("const auto&")
	{
		alignas(16) auto[min, max, sum] = calc_stats(vec);
	}

	SECTION("auto&")
	{
		int coord[3] = { 1, 2, 3 };
		auto&[x, y, z] = coord;

		x = 8;
		REQUIRE(coord[0] == 8);
	}
}

TEST_CASE("Use cases")
{
	map<int, string> dict = { {1, "one"}, {2, "two"}, {3, "three"} };

	SECTION("iteration over map")
	{		
		for (const auto& [key, value] : dict)
		{
			cout << key << " - " << value << endl;
		}
	}

	SECTION("insert into associative container")
	{		
		if (auto[pos, was_inserted] = dict.insert(pair(4, "four")); was_inserted)
		{
			auto&[key, value] = *pos;
			cout << key << " - " << value << " was inserted" << '\n';
		}
		else
		{
			cout << "No insertion..." << '\n';
		}
	}

	SECTION("init for many vars")
	{
		auto[x, y, is_ok] = tuple(1, 3.14, true);

		vector vec = { 665, 42, 31, 14 };

		for (auto [index, pos] = tuple(0, begin(vec)); pos != end(vec); ++index, ++pos)
		{
			cout << index << " - " << *pos << "\n";
		}
	}
}

enum Something {some, thing};

const map<int, string> something_desc = { {0, "some"}, {1, "thing"} };


// step 1 - describe size of tuple-like element
//template <>
//struct std::tuple_size<Something> 
//{
//	static constexpr size_t value = 2;
//};

template <>
struct std::tuple_size<Something> : std::integral_constant<size_t, 2>
{};

// step 2 - describe types of items
template<>
struct std::tuple_element<0, Something>
{
	using type = int;
};

template <>
struct std::tuple_element<1, Something> : std::common_type<const string&>
{};

// step 3 - implementing get<Index>()
template <size_t Index>
decltype(auto) get(const Something& s)
{
	if constexpr (Index == 0)
	{
		return static_cast<int>(s);
	}
	else
	{
		return something_desc.at(static_cast<int>(s));
	}	
}

/*
template <>
decltype(auto) get<0>(const Something& s)
{
	return static_cast<int>(s);
}

template <>
decltype(auto) get<1>(const Something& s)
{
	return something_desc.at(static_cast<int>(s));
}
*/*

TEST_CASE("unpack enum value")
{
	Something sth = some;

	const auto[value, description] = sth;

	REQUIRE(value == 0);
	REQUIRE(description == "some"s);
}