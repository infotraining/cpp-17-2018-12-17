#include <algorithm>
#include <numeric>
#include <iostream>
#include <string>
#include <vector>
#include <future>

#include "catch.hpp"

using namespace std;

class IGadget
{
public:
	virtual void use() const = 0;
	virtual ~IGadget() = default;
};

class Gadget : public IGadget
{
	string name_;
	vector<int> data_;

public:
	void use() const override
	{
		//...
	}	
};

vector<int> create_vector_rvo(size_t n)
{
	return vector<int>(n); // Return Value Optimization - RVO
}

vector<int> create_vector_nrvo(size_t n)
{
	vector<int> vec;

	vec.reserve(1'000'000);

	for (int i = 0; i < 1'000'000; ++i)
		vec.push_back(i);

	return vec; // Named Return Value Optimization - NRVO
}


struct NoCopyableAndNoMoveable
{
	vector<int> vec;

	NoCopyableAndNoMoveable(const NoCopyableAndNoMoveable&) = delete;
	NoCopyableAndNoMoveable& operator=(const NoCopyableAndNoMoveable&) = delete;
	NoCopyableAndNoMoveable(NoCopyableAndNoMoveable&&) = delete;
	NoCopyableAndNoMoveable& operator=(NoCopyableAndNoMoveable&&) = delete;
	~NoCopyableAndNoMoveable() = default;
};

NoCopyableAndNoMoveable create_magic_object()
{
	return NoCopyableAndNoMoveable{ {1, 2, 3} };
}

void foo(NoCopyableAndNoMoveable ncm)
{
	cout << ncm.vec.size() << endl;
}

TEST_CASE("rvo")
{
	auto vec = create_vector_rvo(1'000'000);

	SECTION("for return value")
	{
		NoCopyableAndNoMoveable ncm = create_magic_object();

		REQUIRE(ncm.vec == vector{ 1, 2, 3 });
	}

	SECTION("for argument")
	{
		//foo(NoCopyableAndNoMoveable{ { 5, 6 } });
	}
}

struct Result
{
	string content;
	int err_code;
};

[[ nodiscard ]] Result load_file(const string& file_name)
{
	return Result{ "text", 0 };
}

void save_file(const string& file_name, const string& content)
{
	cout << "Saving a file " << file_name << " - started" << endl;

	this_thread::sleep_for(3s);
	//...
	cout << "Saving a file " << file_name << " - finished" << endl;
}


TEST_CASE("attributes")
{
	if (auto[content, error_code] = load_file("bad_name"); error_code == 0)
	{

	}

	auto f1 = async(launch::async, save_file, "txt1", "content1");
	auto f2 = async(launch::async, save_file, "txt2", "content2");
	auto f3 = async(launch::async, save_file, "txt3", "content3");
}
