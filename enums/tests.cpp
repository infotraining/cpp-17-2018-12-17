#include <algorithm>
#include <numeric>
#include <iostream>
#include <string>
#include <vector>
#include <cstddef>

#include "catch.hpp"

using namespace std;


enum Coffee : uint8_t { espresso, cappucino, latte };

enum class Engine { diesel, dyno, hybrid };

TEST_CASE("enums in C++17")
{
	Coffee coffee{ 1 }; // new feature

	REQUIRE(coffee == cappucino);

	Engine engine{ 1 };

	REQUIRE(engine == Engine::dyno);
}

enum Index : size_t {};

constexpr size_t to_size_t(Index i)
{
	return static_cast<size_t>(i);
}

struct Array
{
	vector<int> items = { 1, 2, 3, 4, 5, 6 };

	int& operator[](Index index)
	{
		return items[to_size_t(index)];
	}
};

TEST_CASE("using array with new index")
{
	Array arr;

	REQUIRE(arr[Index{ 1 }] == 2);
}

TEST_CASE("std::byte")
{
	char a = 13;
	a++;

	std::byte b1{ 13 };

	cout << std::to_integer<int>(b1) << endl;

	b1 = b1 << 2;

	cout << std::to_integer<int>(b1) << endl;
}

TEST_CASE("auto & list init")
{
	auto xa1 = 10;
	int xa2 = 10;

	int xb1{ 10 };
	auto xb2{ 10 }; // std::initializer_list<int> -> int
	//auto xb3{ 1, 2, 3 }; // ill-formed

	auto xc = { 10 }; // std::initializer_list<int>
}

template <typename F>
void call(F f1, F f2)
{
	f1();
	f2();
}

void foo1() {}
void foo2() noexcept {}

TEST_CASE("noexcept as part of function type")
{
	call<void(*)()>(foo1, foo1);
}